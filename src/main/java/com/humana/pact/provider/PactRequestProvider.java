package com.humana.pact.provider;

import com.fasterxml.jackson.core.JsonProcessingException;

import com.humana.pact.model.local.PactLocalRequest;
import com.humana.pact.serialization.PactTransactionSerializer;
import com.humana.pact.model.*;
import com.google.common.io.BaseEncoding;
import com.humana.pact.model.listen.PactListenRequest;
import com.humana.pact.model.send.PactSendRequest;
import net.i2p.crypto.eddsa.EdDSAEngine;
import net.i2p.crypto.eddsa.EdDSAPrivateKey;
import net.i2p.crypto.eddsa.Utils;
import net.i2p.crypto.eddsa.spec.EdDSANamedCurveTable;
import net.i2p.crypto.eddsa.spec.EdDSAParameterSpec;
import net.i2p.crypto.eddsa.spec.EdDSAPrivateKeySpec;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.stereotype.Component;
import ove.crypto.digest.Blake2b;

import javax.xml.ws.WebServiceException;
import java.security.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import static net.i2p.crypto.eddsa.Utils.bytesToHex;
import static net.i2p.crypto.eddsa.Utils.hexToBytes;

@Component
public class PactRequestProvider {
    @Autowired
    PactTransactionSerializer pactTransactionSerializer;

    @Autowired
    PactSignatureProvider pactSignatureProvider;

    @Autowired
    PactCommandHashProvider pactCommandHashProvider;

    public PactListenRequest getListenRequest(String requestKey) {
        return new PactListenRequest() {{
            setRequestKey(requestKey);
        }};
    }

    public PactSendRequest getSendRequest(PactKeyPair keyPair, List<PactTransaction> transactions) throws JsonProcessingException, SignatureException {
        ArrayList<PactCommand> commands = new ArrayList<>();
        for (PactTransaction transaction : transactions) {
            PactCommand command = getPactCommand(keyPair, transaction);
            commands.add(command);
        }

        return new PactSendRequest() {{
            setCommands(commands);
        }};
    }

    public PactSendRequest getSendRequest(List<PactKeyPair> keyPairs, List<PactTransaction> transactions) throws JsonProcessingException, SignatureException {
        ArrayList<PactCommand> commands = new ArrayList<>();
        for (PactTransaction transaction : transactions) {
            PactCommand command = getPactCommand(keyPairs, transaction);
            commands.add(command);
        }

        return new PactSendRequest() {{
           setCommands(commands);
        }};
    }

    public PactLocalRequest getLocalRequest(PactKeyPair keyPair, PactTransaction transaction) throws JsonProcessingException, SignatureException {
        return new PactLocalRequest(getPactCommand(keyPair, transaction));
    }

    public PactLocalRequest getLocalRequest(List<PactKeyPair> keyPairs, PactTransaction transaction) throws JsonProcessingException, SignatureException {
        return new PactLocalRequest(getPactCommand(keyPairs, transaction));
    }

    private PactCommand getPactCommand (List<PactKeyPair> keypairs, PactTransaction transaction) throws JsonProcessingException, SignatureException {
        //cmds
        String pactValue = pactTransactionSerializer.serialize(transaction);
        //hash
        byte[] hash = pactCommandHashProvider.calculateBlake2bHash(pactValue);

        ArrayList<PactSignature> signatures = new ArrayList<>();
        for (PactKeyPair keyPair : keypairs) {
            signatures.add(instantiatePactSignature(keyPair, hash));
        }

        return instantiatePactCommand(pactValue, hash, signatures);
    }

    private PactCommand getPactCommand(PactKeyPair keyPair, PactTransaction transaction) throws JsonProcessingException, SignatureException {
        //cmds
        String pactValue = pactTransactionSerializer.serialize(transaction);
        //hash
        byte[] hash = pactCommandHashProvider.calculateBlake2bHash(pactValue);
        //sigs
        PactSignature signature = instantiatePactSignature(keyPair, hash);

        return instantiatePactCommand(pactValue, hash, Arrays.asList(signature));
    }

    private PactSignature instantiatePactSignature (PactKeyPair keyPair, byte[] hash) throws SignatureException {
        return new PactSignature() {{
            try {
                setValue(pactSignatureProvider.getSignature(keyPair, hash));
            } catch (Exception e) {
                e.printStackTrace();
                throw new SignatureException("Failed to generate pactSignature");
            }
            setScheme("ED25519");
            setPublicKey(keyPair.getPublicKey());
        }};
    }

    private PactCommand instantiatePactCommand (String value, byte[] hash, List<PactSignature> signatures) {
        return new PactCommand() {{
            setValue(value);
            setHash(bytesToHex(hash));
            setSignatures(signatures);
        }};
    }
}

