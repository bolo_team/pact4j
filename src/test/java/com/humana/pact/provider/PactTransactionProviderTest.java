package com.humana.pact.provider;

import com.humana.pact.configuration.Pact4jConfiguration;
import com.humana.pact.model.PactTransaction;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
@ContextConfiguration(classes={ Pact4jConfiguration.class })
public class PactTransactionProviderTest {
    @Autowired
    PactTransactionProvider subject;

    @Test
    public void getTransaction() throws Exception {
        String address = "address";
        HashMap<String, Object> data = new HashMap<>();
        data.put("test", "data");
        String pactCode = "pact code";

        PactTransaction actual = subject.getTransaction(address, data, pactCode);

        assertThat(actual.getPayload().getExec().getData().get("test")).isEqualTo("data");
        assertThat(actual.getPayload().getExec().getCode()).isEqualTo(pactCode);
        assertThat(actual.getNonce()).isNotBlank();
        assertThat(actual.getAddress()).isEqualTo(address);
    }
}