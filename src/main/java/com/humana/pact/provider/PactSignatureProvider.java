package com.humana.pact.provider;

import com.humana.pact.model.PactKeyPair;
import net.i2p.crypto.eddsa.EdDSAEngine;
import net.i2p.crypto.eddsa.EdDSAPrivateKey;
import net.i2p.crypto.eddsa.spec.EdDSANamedCurveTable;
import net.i2p.crypto.eddsa.spec.EdDSAParameterSpec;
import net.i2p.crypto.eddsa.spec.EdDSAPrivateKeySpec;

import java.security.*;

import static net.i2p.crypto.eddsa.Utils.bytesToHex;
import static net.i2p.crypto.eddsa.Utils.hexToBytes;

public class PactSignatureProvider {
    public String getSignature(PactKeyPair keyPair, byte[] digest) throws NoSuchAlgorithmException, InvalidKeyException, InvalidAlgorithmParameterException, SignatureException {
        EdDSAParameterSpec spec = EdDSANamedCurveTable.getByName("Ed25519");
        Signature sgr = new EdDSAEngine(MessageDigest.getInstance(spec.getHashAlgorithm()));

        EdDSAPrivateKeySpec keySpec = new EdDSAPrivateKeySpec(hexToBytes(keyPair.getPrivateKey()), spec);
        sgr.initSign(new EdDSAPrivateKey(keySpec));
        sgr.setParameter(EdDSAEngine.ONE_SHOT_MODE);
        sgr.update(digest);

        return bytesToHex(sgr.sign());
    }
}
