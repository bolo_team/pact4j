package com.humana.pact.provider;

import com.humana.pact.configuration.Pact4jConfiguration;
import com.humana.pact.model.PactKeyPair;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes={ Pact4jConfiguration.class })
@TestPropertySource(properties = {
    "pact.KEYPAIRS={\"some-keypair-name-1\": {\"public_key\": \"public-key-1\", \"private_key\": \"private-key-1\"}, " +
            "\"some-keypair-name-2\": {\"public_key\": \"public-key-2\", \"private_key\": \"private-key-2\"}}"
})
public class PactKeyPairProviderTest {
    @Autowired PactKeyPairProvider subject;

    @Test
    public void getKeyPair() throws IOException {

        PactKeyPair expectedPactKeyPair1 = new PactKeyPair() {{
            setPublicKey("public-key-1");
            setPrivateKey("private-key-1");
        }};

        PactKeyPair expectedPactKeyPair2 = new PactKeyPair() {{
            setPublicKey("public-key-2");
            setPrivateKey("private-key-2");
        }};

        PactKeyPair keyPair1 = subject.getKeyPair("some-keypair-name-1");
        assertThat(keyPair1.getPrivateKey()).isEqualTo(expectedPactKeyPair1.getPrivateKey());
        assertThat(keyPair1.getPublicKey()).isEqualTo(expectedPactKeyPair1.getPublicKey());

        PactKeyPair keyPair2 = subject.getKeyPair("some-keypair-name-2");
        assertThat(keyPair2.getPrivateKey()).isEqualTo(expectedPactKeyPair2.getPrivateKey());
        assertThat(keyPair2.getPublicKey()).isEqualTo(expectedPactKeyPair2.getPublicKey());
    }

}