package com.humana.pact.provider;

import org.springframework.stereotype.Component;
import ove.crypto.digest.Blake2b;

@Component
public class PactCommandHashProvider {
    public byte[] calculateBlake2bHash(String pactValue) {
        return Blake2b.Digest.newInstance().digest(pactValue.getBytes());
    }
}
