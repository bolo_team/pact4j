package com.humana.pact.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class PactCommand {
    @JsonProperty("hash")
    private String hash;

    @JsonProperty("sigs")
    private List<PactSignature> signatures;

    @JsonProperty("cmd")
    private String value;

    public PactCommand() {
        this.signatures = new ArrayList<>();
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public List<PactSignature> getSignatures() {
        return signatures;
    }

    public void setSignatures(List<PactSignature> signatures) {
        this.signatures = signatures;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
