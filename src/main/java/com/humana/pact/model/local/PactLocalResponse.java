package com.humana.pact.model.local;

import com.humana.pact.model.PactResponse;

public class PactLocalResponse<T> extends PactResponse<PactLocalResponseBody<T>> { }
