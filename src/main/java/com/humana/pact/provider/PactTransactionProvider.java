package com.humana.pact.provider;

import com.humana.pact.model.PactExec;
import com.humana.pact.model.PactPayload;
import com.humana.pact.model.PactTransaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Component
public class PactTransactionProvider{
    public PactTransaction getTransaction(String address, Map data, String code) {
        return new PactTransaction() {{
            setAddress(address);
            setPayload(new PactPayload(){{
                setExec(new PactExec() {{
                    setData(data);
                    setCode(code);
                }});
            }});
            setNonce(UUID.randomUUID().toString());
        }};
    }
}
