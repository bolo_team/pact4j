package com.humana.pact.model.listen;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PactListenRequest {
    @JsonProperty("listen")
    private String requestKey;

    public String getRequestKey() {
        return requestKey;
    }

    public void setRequestKey(String requestKey) {
        this.requestKey = requestKey;
    }
}
