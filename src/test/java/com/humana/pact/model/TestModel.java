package com.humana.pact.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TestModel {
    @JsonProperty("test")
    public String test;
}
