package com.humana.pact.provider;

import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Component;

@Component
public class HttpEntityProvider<T> {
    public HttpEntity<T> getRequestEntity(T request) {
        return new HttpEntity<>(request);
    }
}
