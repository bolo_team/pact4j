package com.humana.pact.provider;

import com.humana.pact.configuration.Pact4jConfiguration;
import com.humana.pact.model.PactKeyPair;
import com.humana.pact.model.PactTransaction;
import com.humana.pact.model.listen.PactListenRequest;
import com.humana.pact.model.local.PactLocalRequest;
import com.humana.pact.model.send.PactSendRequest;
import com.humana.pact.serialization.PactTransactionSerializer;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes={ Pact4jConfiguration.class })
public class PactRequestProviderTest {
    @MockBean
    private PactTransactionSerializer mockPactTransactionSerializer;

    @MockBean
    private PactSignatureProvider mockPactSignatureProvider;

    @MockBean
    private PactCommandHashProvider mockPactCommandHashProvider;

    @Autowired
    private PactRequestProvider subject;

    @Mock
    private PactKeyPair mockPactKeyPair;

    @Mock
    private PactTransaction mockPactTransaction;

    private List<PactTransaction> pactTransactions;
    private List<PactKeyPair> pactkeyPairs;

    @Before
    public void setUp() {
        pactTransactions = new ArrayList<PactTransaction>() {{
            add(mockPactTransaction);
        }};

        pactkeyPairs = new ArrayList<PactKeyPair>() {{
            add(mockPactKeyPair);
            add(mockPactKeyPair);
        }};
    }

    @Test
    public void getListenRequest() throws Exception {
        String requestKey = "requestKey";
        PactListenRequest actual = subject.getListenRequest(requestKey);
        assertThat(actual).isInstanceOf(PactListenRequest.class);
        assertThat(actual.getRequestKey()).isEqualToIgnoringWhitespace(requestKey);
    }

    @Test
    public void getSendRequest_withSingleKeyPair() throws Exception {
        String serializedPactTransactionValue = "serializedPactTransactionValue";
        String pactSignatureOfCommandHashValue = "pactSignatureOfCommandHashValue";

        when(mockPactTransactionSerializer.serialize(mockPactTransaction)).thenReturn(serializedPactTransactionValue);
        byte[] commandHash = new byte[0];
        when(mockPactCommandHashProvider.calculateBlake2bHash(serializedPactTransactionValue)).thenReturn(commandHash);
        when(mockPactSignatureProvider.getSignature(mockPactKeyPair, commandHash)).thenReturn(pactSignatureOfCommandHashValue);

        PactSendRequest actual = subject.getSendRequest(mockPactKeyPair, pactTransactions);
        assertThat(actual).isInstanceOf(PactSendRequest.class);
        assertThat(actual).isNotNull();
        assertThat(actual.getCommands().get(0).getSignatures().get(0).getValue()).isEqualToIgnoringWhitespace(pactSignatureOfCommandHashValue);
        assertThat(actual.getCommands().get(0).getValue()).isEqualToIgnoringWhitespace(serializedPactTransactionValue);
    }

    @Test
    public void getSendRequest_withMultipleKeyPairs() throws Exception {
        String serializedPactTransactionValue = "serializedPactTransactionValue";
        String pactSignatureOfCommandHashValue = "pactSignatureOfCommandHashValue";

        when(mockPactTransactionSerializer.serialize(mockPactTransaction)).thenReturn(serializedPactTransactionValue);
        byte[] commandHash = new byte[0];
        when(mockPactCommandHashProvider.calculateBlake2bHash(serializedPactTransactionValue)).thenReturn(commandHash);
        when(mockPactSignatureProvider.getSignature(mockPactKeyPair, commandHash)).thenReturn(pactSignatureOfCommandHashValue);

        PactSendRequest actual = subject.getSendRequest(pactkeyPairs, pactTransactions);
        assertThat(actual).isInstanceOf(PactSendRequest.class);
        assertThat(actual).isNotNull();
        assertThat(actual.getCommands().get(0).getSignatures().size()).isEqualTo(2);
        assertThat(actual.getCommands().get(0).getSignatures().get(0).getValue()).isEqualToIgnoringWhitespace(pactSignatureOfCommandHashValue);
        assertThat(actual.getCommands().get(0).getSignatures().get(1).getValue()).isEqualToIgnoringWhitespace(pactSignatureOfCommandHashValue);
        assertThat(actual.getCommands().get(0).getValue()).isEqualToIgnoringWhitespace(serializedPactTransactionValue);
    }

    @Test
    public void getLocalRequest_withSingleKeyPair() throws Exception {
        String serializedPactTransactionValue = "g348hw3489gq343489gq39phpw4343h";
        String pactSignatureOfCommandHashValue = "jf930j90f32jf32j9f0329fj03f3aj9f32j9fj392j9f3j9f3";

        when(mockPactTransactionSerializer.serialize(mockPactTransaction)).thenReturn(serializedPactTransactionValue);
        byte[] commandHash = new byte[0];
        when(mockPactCommandHashProvider.calculateBlake2bHash(serializedPactTransactionValue)).thenReturn(commandHash);
        when(mockPactSignatureProvider.getSignature(mockPactKeyPair, commandHash)).thenReturn(pactSignatureOfCommandHashValue);

        PactLocalRequest actual = subject.getLocalRequest(mockPactKeyPair, mockPactTransaction);
        assertThat(actual).isInstanceOf(PactLocalRequest.class);
        assertThat(actual.getSignatures().get(0).getValue()).isEqualTo(pactSignatureOfCommandHashValue);
        assertThat(actual.getValue()).isEqualTo(serializedPactTransactionValue);
    }

    @Test
    public void getLocalRequest_withMulitpleKeyPairs() throws Exception {
        String serializedPactTransactionValue = "g348hw3489gq343489gq39phpw4343h";
        String pactSignatureOfCommandHashValue = "jf930j90f32jf32j9f0329fj03f3aj9f32j9fj392j9f3j9f3";

        when(mockPactTransactionSerializer.serialize(mockPactTransaction)).thenReturn(serializedPactTransactionValue);
        byte[] commandHash = new byte[0];
        when(mockPactCommandHashProvider.calculateBlake2bHash(serializedPactTransactionValue)).thenReturn(commandHash);
        when(mockPactSignatureProvider.getSignature(mockPactKeyPair, commandHash)).thenReturn(pactSignatureOfCommandHashValue);

        PactLocalRequest actual = subject.getLocalRequest(pactkeyPairs, mockPactTransaction);
        assertThat(actual).isInstanceOf(PactLocalRequest.class);
        assertThat(actual.getSignatures().size()).isEqualTo(2);
        assertThat(actual.getSignatures().get(0).getValue()).isEqualTo(pactSignatureOfCommandHashValue);
        assertThat(actual.getSignatures().get(1).getValue()).isEqualTo(pactSignatureOfCommandHashValue);
        assertThat(actual.getValue()).isEqualTo(serializedPactTransactionValue);
    }
}