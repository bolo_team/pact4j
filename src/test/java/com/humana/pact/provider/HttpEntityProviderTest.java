package com.humana.pact.provider;

import com.humana.pact.configuration.Pact4jConfiguration;
import com.humana.pact.model.TestModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes={ Pact4jConfiguration.class })
public class HttpEntityProviderTest {
    @Autowired
    private HttpEntityProvider subject;

    @Test
    public void getRequestEntity() {
        TestModel request = new TestModel();
        assertThat(subject.getRequestEntity(request).getBody()).isOfAnyClassIn(TestModel.class);
    }
}