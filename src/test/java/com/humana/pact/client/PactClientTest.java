package com.humana.pact.client;

import com.fasterxml.jackson.core.type.TypeReference;
import com.humana.pact.configuration.Pact4jConfiguration;
import com.humana.pact.model.TestModel;
import com.humana.pact.model.listen.PactListenRequest;
import com.humana.pact.model.listen.PactListenResponse;
import com.humana.pact.model.local.PactLocalRequest;
import com.humana.pact.model.local.PactLocalResponse;
import com.humana.pact.model.send.PactSendRequest;
import com.humana.pact.model.send.PactSendResponse;
import com.humana.pact.provider.HttpEntityProvider;
import com.humana.pact.serialization.PactResultDeserializer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes={ Pact4jConfiguration.class })
@TestPropertySource(properties = { "pact.baseUrl=http://test-base.url/" })
public class PactClientTest {
    @MockBean
    private RestTemplate restTemplate;

    @MockBean
    private HttpEntityProvider httpEntityProvider;

    @MockBean
    private PactResultDeserializer pactResultDeserializer;

    @Mock
    private HttpEntity mockRequestEntity;

    @Mock
    private PactSendResponse sendResponse;

    @Mock
    private ResponseEntity<PactSendResponse> mockResponseEntity;

    @Mock
    private PactListenResponse listenResponse;

    @Mock
    private ResponseEntity<PactListenResponse> listenResponseEntity;

    @Mock
    private PactLocalResponse localResponse;

    @Mock
    private ResponseEntity<PactLocalResponse> mockLocalResponseEntity;

    @Mock
    private PactListenRequest mockListenRequest;

    @Mock
    private PactLocalRequest mockLocalRequest;

    @Autowired
    private PactClient subject;

    @Test
    public void testSend() {
        PactSendRequest request = new PactSendRequest();

        when(httpEntityProvider.getRequestEntity(request)).thenReturn(mockRequestEntity);

        when(restTemplate.exchange("http://test-base.url/api/v1/send", HttpMethod.POST, mockRequestEntity, PactSendResponse.class))
                .thenReturn(mockResponseEntity);

        when(mockResponseEntity.getBody()).thenReturn(sendResponse);

        assertThat(subject.send(request)).isSameAs(sendResponse);
    }

    @Test
    public void testListen() {
        when(httpEntityProvider.getRequestEntity(mockListenRequest)).thenReturn(mockRequestEntity);

        when(restTemplate.exchange("http://test-base.url/api/v1/listen", HttpMethod.POST, mockRequestEntity, PactListenResponse.class))
                .thenReturn(listenResponseEntity);

        when(listenResponseEntity.getBody()).thenReturn(listenResponse);

        assertThat(subject.listen(mockListenRequest)).isSameAs(listenResponse);
    }

    @Test
    public void testLocal_withObjectClass() {
        when(httpEntityProvider.getRequestEntity(mockLocalRequest)).thenReturn(mockRequestEntity);
        when(restTemplate.exchange("http://test-base.url/api/v1/local", HttpMethod.POST, mockRequestEntity, PactLocalResponse.class))
                .thenReturn(mockLocalResponseEntity);
        when(mockLocalResponseEntity.getBody()).thenReturn(localResponse);

        PactLocalResponse mockLocalResponse = mock(PactLocalResponse.class);
        when(pactResultDeserializer.deserializeLocalResponse(localResponse, TestModel.class)).thenReturn(mockLocalResponse);

        assertThat(subject.local(mockLocalRequest, TestModel.class)).isSameAs(mockLocalResponse);
    }

    @Test
    public void testLocal_withParameterizedTypeReference() {
        when(httpEntityProvider.getRequestEntity(mockLocalRequest)).thenReturn(mockRequestEntity);
        when(restTemplate.exchange("http://test-base.url/api/v1/local", HttpMethod.POST, mockRequestEntity, PactLocalResponse.class))
                .thenReturn(mockLocalResponseEntity);
        when(mockLocalResponseEntity.getBody()).thenReturn(localResponse);

        PactLocalResponse mockLocalResponse = mock(PactLocalResponse.class);
        TypeReference testModelListTypeRef = mock(TypeReference.class);
        when(pactResultDeserializer.deserializeLocalResponse(localResponse, testModelListTypeRef)).thenReturn(mockLocalResponse);

        assertThat(subject.local(mockLocalRequest, testModelListTypeRef)).isSameAs(mockLocalResponse);
    }
}