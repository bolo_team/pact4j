package com.humana.pact.provider;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.humana.pact.model.PactKeyPair;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import java.io.IOException;
import java.util.Map;

@Service
public class PactKeyPairProvider {
    @Value(value = "${pact.KEYPAIRS}")
    private String pactKeyPairsString;

    public PactKeyPair getKeyPair(String name) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        TypeReference<Map<String, PactKeyPair>> typeRef = new TypeReference<Map<String, PactKeyPair>>() {};

        Map pactKeyPairMap = mapper.readValue(pactKeyPairsString, typeRef);
        return (PactKeyPair) pactKeyPairMap.get(name);
    }
}
