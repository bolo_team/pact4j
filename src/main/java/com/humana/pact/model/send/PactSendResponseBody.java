package com.humana.pact.model.send;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class PactSendResponseBody {
    @JsonProperty("requestKeys")
    private List<String> requestKeys;

    public List<String> getRequestKeys() {
        return requestKeys;
    }

    public void setRequestKeys(List<String> requestKeys) {
        this.requestKeys = requestKeys;
    }
}
