package com.humana.pact.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PactTransactionLogItem<T> {
    @JsonProperty("txid")
    private long txid;
    @JsonProperty("value")
    private T value;

    public long getTxid() {
        return txid;
    }

    public void setTxid(long txid) {
        this.txid = txid;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }
}

