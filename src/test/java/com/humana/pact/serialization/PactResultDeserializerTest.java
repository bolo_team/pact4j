package com.humana.pact.serialization;

import com.fasterxml.jackson.core.type.TypeReference;
import com.humana.pact.configuration.Pact4jConfiguration;
import com.humana.pact.model.TestModel;
import com.humana.pact.model.local.PactLocalResponse;
import com.humana.pact.model.local.PactLocalResponseBody;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes={ Pact4jConfiguration.class })
public class PactResultDeserializerTest {
    @Autowired
    private PactResultDeserializer subject;

    @Test
    public void deserializeLocalResponse_withModelClass() throws Exception {
        PactLocalResponse localResponse = new PactLocalResponse();
        localResponse.setBody(new PactLocalResponseBody());
        LinkedHashMap<String, String> hashMap = new LinkedHashMap<>();
        hashMap.put("test", "value");

        ((PactLocalResponseBody)localResponse.getBody()).setData(hashMap);

        PactLocalResponse<TestModel> actual = subject.deserializeLocalResponse(localResponse, TestModel.class);

        PactLocalResponseBody responseBody = actual.getBody();
        assertThat(responseBody.getData()).isOfAnyClassIn(TestModel.class);
        assertThat(responseBody.getData()).hasFieldOrProperty("test");
        assertThat(((TestModel)responseBody.getData()).test).isEqualTo("value");
    }

    @Test
    public void deserializeLocalResponse_withParameterizedTypeReference() throws Exception {
        PactLocalResponse localResponse = new PactLocalResponse();
        localResponse.setBody(new PactLocalResponseBody());
        LinkedList<LinkedHashMap<String, String>> arrayOfTestObjects = new LinkedList() {{
            add(new LinkedHashMap<String, String>() {{
                put("test", "value-1");
            }});
            add(new LinkedHashMap<String, String>() {{
                put("test", "value-2");
            }});
        }};

        ((PactLocalResponseBody)localResponse.getBody()).setData(arrayOfTestObjects);

        TypeReference<List<TestModel>> listOfTestModelTypeRef = new TypeReference<List<TestModel>>() {};
        PactLocalResponse<List<TestModel>> actual = subject.deserializeLocalResponse(localResponse, listOfTestModelTypeRef);

        PactLocalResponseBody responseBody = actual.getBody();
        assertThat(responseBody.getData()).isInstanceOfAny(List.class);
        assertThat(((List)responseBody.getData()).get(0)).hasFieldOrProperty("test");
        assertThat(((TestModel)((List)responseBody.getData()).get(0)).test).isEqualTo("value-1");
        assertThat(((TestModel)((List)responseBody.getData()).get(1)).test).isEqualTo("value-2");
    }

}