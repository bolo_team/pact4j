package com.humana.pact.serialization;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.humana.pact.model.PactTransaction;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

@Component
public class PactTransactionSerializer {
    public String serialize(PactTransaction transaction) throws JsonProcessingException {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        df.setTimeZone(TimeZone.getTimeZone("Etc/Zulu"));
        ObjectMapper mapper = new ObjectMapper();
        mapper.setDateFormat(df);
        return mapper.writeValueAsString(transaction);
    }
}
