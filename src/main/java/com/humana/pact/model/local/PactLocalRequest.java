package com.humana.pact.model.local;

import com.humana.pact.model.PactCommand;

public class PactLocalRequest extends PactCommand {
    public PactLocalRequest(PactCommand pactCommand) {
        this.setHash(pactCommand.getHash());
        this.setSignatures(pactCommand.getSignatures());
        this.setValue(pactCommand.getValue());
    }
}
