package com.humana.pact.provider;

import com.humana.pact.configuration.Pact4jConfiguration;
import com.humana.pact.model.PactKeyPair;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static net.i2p.crypto.eddsa.Utils.hexToBytes;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes={ Pact4jConfiguration.class })
public class PactSignatureProviderTest {
    @Autowired
    PactSignatureProvider subject;

    @Test
    public void getSignature() throws Exception {
        byte[] bytes = hexToBytes("aa0eeeedfedc3af0c052eb13afaee1ac92fdfd6169c9f7da75173b3c1909fc6f0159d76be3dd970970d1f47af1fe483957271b0ae98af139b1ce74214a900f84");

        PactKeyPair keypair = new PactKeyPair() {{
            setPrivateKey("ae29addb10217473d3e0eb9471ed22c9e30511cc096789b63fc95ae4375b96dc");
            setPublicKey("5bcd51d883bf977ab244383f242a6ecb2d5edfc8e4647e75a7706a73cfac2d5b");
        }};
        assertThat(subject.getSignature(keypair, bytes)).isEqualTo("dc1b6780d7279e2937dc66f2473bac192dcfd6816aff058bbcea71d7ab73da06aeeddc5a516e721bc318121e01644f179986cc3721d8352976e97e55ac3dcd0f");
    }
}