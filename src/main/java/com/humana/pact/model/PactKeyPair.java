package com.humana.pact.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PactKeyPair {
    @JsonProperty("public_key")
    private String publicKey;

    @JsonProperty("private_key")
    private String privateKey;

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }
}
