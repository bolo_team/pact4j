package com.humana.pact.model.listen;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.humana.pact.model.local.PactLocalResponseBody;

public class PactListenResponseBody {
    @JsonProperty("txId")
    private String transactionId;

    @JsonProperty("result")
    private PactListenResponseBodyResult result;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public PactListenResponseBodyResult getResult() {
        return result;
    }

    public void setResult(PactListenResponseBodyResult result) {
        this.result = result;
    }
}