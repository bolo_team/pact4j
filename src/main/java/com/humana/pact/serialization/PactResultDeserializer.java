package com.humana.pact.serialization;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.humana.pact.model.local.PactLocalResponse;
import com.humana.pact.model.local.PactLocalResponseBody;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.annotation.Order;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;

public class PactResultDeserializer {
    public <T> PactLocalResponse <T> deserializeLocalResponse(PactLocalResponse response, Class<T> testModelClass) {
        ObjectMapper mapper = new ObjectMapper();
        PactLocalResponseBody body = (PactLocalResponseBody) response.getBody();
        body.setData(mapper.convertValue(body.getData(), testModelClass));
        return response;
    }

    public <T> PactLocalResponse <T>deserializeLocalResponse(PactLocalResponse response, TypeReference<T> responseType) {
        ObjectMapper mapper = new ObjectMapper();
        PactLocalResponseBody body = (PactLocalResponseBody) response.getBody();
        Object data = mapper.convertValue(body.getData(), responseType);
        body.setData(data);
        return response;
    }
}
