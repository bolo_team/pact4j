package com.humana.pact.configuration;

import com.humana.pact.client.PactClient;
import com.humana.pact.model.PactKeyPair;
import com.humana.pact.provider.*;
import com.humana.pact.serialization.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class Pact4jConfiguration {
    @Value(value = "${pact.baseUrl}")
    private String pactBaseUrlFromEnvironment;

    @Bean
    PactSignatureProvider pactSignatureProvider() {
        return new PactSignatureProvider();
    }

    @Bean
    PactCommandHashProvider pactCommandHashProvider() {
        return new PactCommandHashProvider();
    }

    @Bean
    PactTransactionProvider pactTransactionProvider() {
        return new PactTransactionProvider();
    }

    @Bean
    PactRequestProvider pactRequestProvider() {
        return new PactRequestProvider();
    }

    @Bean
    PactTransactionSerializer pactTransactionSerializer() {
        return new PactTransactionSerializer();
    }

    @Bean
    HttpEntityProvider httpEntityProvider() {
        return new HttpEntityProvider();
    }

    @Bean
    RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    PactResultDeserializer pactResultDeserializer() {
        return new PactResultDeserializer();
    }

    @Bean
    PactKeyPairProvider pactKeyPairProvider() { return new PactKeyPairProvider(); }

    @Bean
    PactClient pactClient() {
        return new PactClient() {{
            setBaseUrl(pactBaseUrlFromEnvironment);
        }};
    }
}
