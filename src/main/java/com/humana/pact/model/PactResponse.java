package com.humana.pact.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public abstract class PactResponse<B> {
    @JsonProperty("status")
    private String status;

    @JsonProperty("response")
    private B body;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public B getBody() {
        return body;
    }

    public void setBody(B body) {
        this.body = body;
    }
}