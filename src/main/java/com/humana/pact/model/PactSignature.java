package com.humana.pact.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PactSignature {
    @JsonProperty("sig")
    private String value;

    @JsonProperty("scheme")
    private String scheme;

    @JsonProperty("pubKey")
    private String publicKey;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getScheme() {
        return scheme;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }
}
