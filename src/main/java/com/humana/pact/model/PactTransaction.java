package com.humana.pact.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PactTransaction {
    @JsonProperty("address")
    private String address;

    @JsonProperty("payload")
    private PactPayload payload;

    @JsonProperty("nonce")
    private String nonce;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public PactPayload getPayload() {
        return payload;
    }

    public void setPayload(PactPayload payload) {
        this.payload = payload;
    }

    public String getNonce() {
        return nonce;
    }

    public void setNonce(String nonce) {
        this.nonce = nonce;
    }
}
