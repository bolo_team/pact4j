package com.humana.pact.serialization;

import com.humana.pact.configuration.Pact4jConfiguration;
import com.humana.pact.model.PactExec;
import com.humana.pact.model.PactPayload;
import com.humana.pact.model.PactTransaction;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes={ Pact4jConfiguration.class })
public class PactTransactionSerializerTest {
    @Autowired
    PactTransactionSerializer subject;

    @Test
    public void serialize() throws Exception {
        PactTransaction pactTransaction = new PactTransaction() {{
            setNonce("nonce");
            setPayload(new PactPayload() {{
                setExec(new PactExec() {{
                    setCode("some-pact-code");
                    setData(new HashMap<String, Object>() {{
                        put("test", "data");
                    }});
                }});
            }});
            setAddress("address");
        }};

        String expected = "{" +
                "\"address\":\"address\"," +
                "\"payload\":{" +
                    "\"exec\":{" +
                        "\"code\":\"some-pact-code\"," +
                        "\"data\":{" +
                            "\"test\":\"data\"}" +
                        "}" +
                    "}," +
                "\"nonce\":\"nonce\"}";

        assertThat(subject.serialize(pactTransaction)).isEqualToIgnoringWhitespace(expected);
    }
}