package com.humana.pact.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PactPayload {
    @JsonProperty("exec")
    private PactExec exec;

    public PactExec getExec() {
        return exec;
    }

    public void setExec(PactExec exec) {
        this.exec = exec;
    }
}

