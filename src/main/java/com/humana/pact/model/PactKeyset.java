package com.humana.pact.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class PactKeyset {
    @JsonProperty("keys")
    private List<String> keys;
    @JsonProperty("pred")
    private String predicate;

    public List<String> getKeys() {
        return keys;
    }

    public void setKeys(List<String> keys) {
        this.keys = keys;
    }

    public String getPredicate() {
        return predicate;
    }

    public void setPredicate(String predicate) {
        this.predicate = predicate;
    }
}
