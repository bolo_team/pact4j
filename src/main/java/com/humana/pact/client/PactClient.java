package com.humana.pact.client;

import com.fasterxml.jackson.core.type.TypeReference;
import com.humana.pact.model.listen.PactListenRequest;
import com.humana.pact.model.listen.PactListenResponse;
import com.humana.pact.model.local.PactLocalRequest;
import com.humana.pact.model.local.PactLocalResponse;
import com.humana.pact.model.send.PactSendRequest;
import com.humana.pact.model.send.PactSendResponse;
import com.humana.pact.provider.HttpEntityProvider;
import com.humana.pact.serialization.PactResultDeserializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class PactClient {
    private String baseUrl;

    private static final String sendApiMethod = "api/v1/send";
    private static final String listenApiMethod = "api/v1/listen";
    private static final String localApiMethod = "api/v1/local";

    @Autowired
    HttpEntityProvider httpEntityProvider;

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    PactResultDeserializer pactResultDeserializer;

    public PactSendResponse send(PactSendRequest createOrderPactSendRequest) {
        HttpEntity<PactSendRequest> entity = httpEntityProvider.getRequestEntity(createOrderPactSendRequest);
        ResponseEntity<PactSendResponse> responseEntity =
                restTemplate.exchange(String.format("%s%s", baseUrl, sendApiMethod), HttpMethod.POST, entity, PactSendResponse.class);
        return responseEntity.getBody();
    }

    public PactListenResponse listen(PactListenRequest listenRequest) {
        HttpEntity<PactListenRequest> entity = httpEntityProvider.getRequestEntity(listenRequest);
        ResponseEntity<PactListenResponse> responseEntity =
                restTemplate.exchange(String.format("%s%s", baseUrl, listenApiMethod), HttpMethod.POST, entity, PactListenResponse.class);
        return responseEntity.getBody();
    }

    public <T> PactLocalResponse <T> local(PactLocalRequest request, Class<T> modelClass) {
        HttpEntity<PactLocalRequest> entity = httpEntityProvider.getRequestEntity(request);
        PactLocalResponse response =
                restTemplate.exchange(String.format("%s%s", baseUrl, localApiMethod), HttpMethod.POST, entity, PactLocalResponse.class).getBody();

        return pactResultDeserializer.deserializeLocalResponse(response, modelClass);
    }

    public <T> PactLocalResponse <T> local(PactLocalRequest request, TypeReference<T> responseType) {
        HttpEntity<PactLocalRequest> entity = httpEntityProvider.getRequestEntity(request);
        PactLocalResponse response =
                restTemplate.exchange(
                        String.format("%s%s", baseUrl, localApiMethod),
                        HttpMethod.POST,
                        entity,
                        PactLocalResponse.class).getBody();

        return pactResultDeserializer.deserializeLocalResponse(response, responseType);
    }

    protected String getBaseUrl() {
        return baseUrl;
    }

    protected void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

}
