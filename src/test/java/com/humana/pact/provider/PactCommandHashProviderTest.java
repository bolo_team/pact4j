package com.humana.pact.provider;

import com.google.common.io.BaseEncoding;
import com.humana.pact.configuration.Pact4jConfiguration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;

import static net.i2p.crypto.eddsa.Utils.bytesToHex;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes={ Pact4jConfiguration.class })
public class PactCommandHashProviderTest {
    @Autowired
    PactCommandHashProvider subject;

    private HashMap<String, String> testValues = new HashMap<String, String>() {{
       put("test-str", "8bd698b5908e00232f7da150a4ef3dcd75754929af4e4433f838fe9d4e2d57cc3f0ce28666c0f38cd756e76348f69d358ed77beb5e874243a42c3d9f01cc64e7");
       put("test-str  ", "6021abb788a28f69e4233fe20904bdd5c9946fd8df469b91f626c1b9b237c2ce848e4a638e626e795d589e7c65b5425fc74d0d2a2d054a10ce4f2f28a25cdc21");
       put("  test_str", "094ab0901af30aad9e88488ef7b3c9eee1729c1330ba0f3c17547be07ca95addba6d0f601ac9f6031e29a1088ba91e8c5db71afb6c27d11cd01f6cdebed0708b");
       put(" test-str", "d2b11a914969f5ec9afcfc151a7358a3d754a8b4a93281237199e2019d9321110f0931b24005abb5cd135352036b6fe629264fc202db24dbff826808b9881ec5");
       put("test*str", "9ebd97c57fbf467ad3dc3750b68fdaa346b0184a8ed1569ed848bf36aedd2fb2fa337f71b2d988840023043fe97f07b3847975acbb88d7d898b01c5756b8ebe9");
       put("test&str", "3bac300b80d34e8a9be88bf24ea48ac6f1f8645d145b4b69f805cd012bd2f90d5172ecd03b6b1fee07d779f18af7b743a40884dc7ae31fc2c2330609ffc5d1c7");
       put("test  str", "774878989030eda5b408bb9ba246f713ee188d0b3e4ddbf3eacab85a2fe969680e1953af1094e7ad54a4284cff562ccb3317405d9ed413e32242ec7e8105d81f");
       put("test str", "591a156401f44cf21045c0c6b3c7159701c396ee17a261a75fb0e01997414404a8aa6537efbfde33b750d8fd33a410c1dd1e9568050c14087526dab449bd5726");
       put("^test-str", "83d8a75eea365c810e04512adea8dd80451234dbb9263f57bde34b5f77ade1e5b8200316a938761c480552c49504109c0291591fc29308ee096f21e80d5fcc8d");
       put("tesstr", "59b6cdeca5accf52dc8ebd30b5f71846573f7178358ca283208524403f661ea36554fef7c479d5be958e0a811fd3995e860277c9c65f773e1601c12e7768d6bd");
       put("teststr ", "0709c20494b634ee4a6f5fcd3aa3844dfa3a31e8509a814aaf1ed6f357dbf00e8dc48ceeca9ff5c168828357b2d92456369dd4f55adc2131a502d32e20226498");
       put("test-str*", "f559eb24ef6a0423625d2778d806f8362db11c5c256a7437fcf3bb0c16cb4d7540cd0de577c49521be07fcfb002d45d6dfb6f0350a640654c393f43234c71e7f");
    }};

    @Test
    public void calculateBlake2bHash() throws Exception {
        for (String key : testValues.keySet()) {
            byte[] hashBytes = subject.calculateBlake2bHash(key);
            assertThat(bytesToHex(hashBytes)).isEqualTo(testValues.get(key));
            assertThat(BaseEncoding.base16().lowerCase().encode(hashBytes)).isEqualTo(testValues.get(key));
        }
    }
}