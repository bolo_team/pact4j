package com.humana.pact.model.send;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.humana.pact.model.PactCommand;

import java.util.ArrayList;
import java.util.List;

public class PactSendRequest {
    @JsonProperty("cmds")
    private List<PactCommand> commands;

    public PactSendRequest () {
        this.commands = new ArrayList<>();
    }

    public List<PactCommand> getCommands() {
        return commands;
    }

    public void setCommands(List<PactCommand> commands) {
        this.commands = commands;
    }
}

